Para executar o projeto, abra o terminal e clone o projeto do seguinte repositório:
git clone https://gitlab.com/Henrique94/EP2_OO.git
Feito isso, entre na IDE netbeans, escolha a opção abrir projeto e selecione o caminho para onde o projeto foi clonado.
Após abrir o projeto nesta IDE, entre em Restaurante, Pacotes de Códigos-fontes, Restaurante_OO, clique com o botão direito
em cima da classe Login.java e escolha a opção Executar Arquivo. Feito isso abrirá uma tela de login, para entra no sistema digite:

Nome:henrique
Senha:123456

A janela principal do programa irá abrir. Para escolher alguma comida disponpivel no sistema insira primeiro a quantidade desejada e em 
seguinda dê um check na caixa de seleção da comida correspondente. Caso deseja inseririr alguma observação basta escrever no campo de texto
em seguida. Caso queira inserir uma taxa de serviço de um check em taxa (valor pré cadastrado de 2,50). Após realizar todos os pedidos
clique em "Gerar Pedido" e uma listagem será apresentado ao lado direito na aba Recibo. Ao final do processo, clique em "Total" para 
visualizar o valor total da conta. Selecione uma forma de pagamento e inisra o valor recebido do cliente, logo em seguida clique em 
"Ver Troco" para visualizar o troco a ser entregue para o cliente.Caso queira recomeçar um novo pedido, clique em reiniciar para zerar os 
valores dos campo de texto. O botão "Sair" fecha o progrma. O software contém aida uma calculadora convecional disponível no lado direito na 
aba calculadora, que realiza as operações básicas. Uma outra funcionalidade do software é a opção de Atualizar Estoque, entretanto apenas a 
interface foi projetada, caso queira visualizar basta clicar em "Atualizar Estoque" e para sair da mesma clique em "Salvar".

